
const muda_cor = () => {
    let cor_selecionada = document.getElementById('color-input').value;
    document.getElementById('muda-cor').style.backgroundColor = cor_selecionada;
}

document.getElementById('color-input').addEventListener('change', muda_cor)


let montadoras = ["Volkswagen", "Toyota", "Honda", "BMW", "Audi"];

const select_montadoras = document.getElementById("select-montadoras");

const atualizaSelectMontadoras = (qntd) => {
    select_montadoras.innerHTML = `
    <option selected disabled value="">
    Escolha uma montadora
    </option>
    `
    for (let i = 0; i < qntd; i++) {
        select_montadoras.innerHTML += `
        <option value="${i}">${montadoras[i]}</option>
        `
    }
}

const quantidade_montadoras = document.getElementById("input-quantidade");

function atualizaInputQuantidade() {
    quantidade_montadoras.setAttribute("max", montadoras.length);
    quantidade_montadoras.setAttribute("min", 0);
    quantidade_montadoras.value = montadoras.length;
}

const ins_montadora = document.getElementById("bt-nova-montadora");
const nova_montadora = document.getElementById("input-nova-montadora");
const escolheu_montadora = document.getElementById("montadora-escolhida")

ins_montadora.addEventListener("click", ()=>{
    if(nova_montadora.value.trim().length < 1){
        alert("Informe uma montadora.");
    } else {
        montadoras.push(nova_montadora.value.trim());
        nova_montadora.value = "";
        atualizaInputQuantidade();
        atualizaSelectMontadoras(quantidade_montadoras.value);
    }
    escolheu_montadora.innerText = "Escolha uma montadora";
}) 

window.onload = ()=>{
    atualizaInputQuantidade();
    atualizaSelectMontadoras(quantidade_montadoras.value);
}

quantidade_montadoras.addEventListener("change", (e)=>{
    atualizaSelectMontadoras(e.target.value);
})

select_montadoras.addEventListener("change", (e)=>{
    escolheu_montadora.innerText = montadoras[e.target.value];
})

const excluir_montadora = document.getElementById("bt-excluir-montadora");

excluir_montadora.addEventListener("click", ()=>{
    if (montadoras.length == 0) {
        alert("Não há mais montadoras!");
    } else {
        montadoras.pop();
        atualizaInputQuantidade();
        atualizaSelectMontadoras(quantidade_montadoras.value);
    }
    escolheu_montadora.innerText = "Escolha uma montadora";
}) 
